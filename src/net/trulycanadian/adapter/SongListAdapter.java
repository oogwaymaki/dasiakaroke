package net.trulycanadian.adapter;

import net.trulycanadian.dasiakaroke.R;
import net.trulycanadian.dasiakaroke.model.SongSummary;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SongListAdapter extends ArrayAdapter<SongSummary> {
	private final Context context;
	private final SongSummary[] values;

	public SongListAdapter(Context context, SongSummary[] songSummaries) {
		super(context, R.layout.show_song_info, songSummaries);
		this.context = context;
		this.values = songSummaries;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.show_song_info, parent, false);
		TextView songName = (TextView) rowView.findViewById(R.id.songName);
		TextView albumName = (TextView) rowView.findViewById(R.id.albumName);
		TextView artistName = (TextView) rowView.findViewById(R.id.artist);
		TextView genre = (TextView) rowView.findViewById(R.id.genre);
		TextView fileName = (TextView) rowView.findViewById(R.id.fileName);
		songName.setText(values[position].getSongName());
		albumName.setText(values[position].getAlbumName());
		artistName.setText(values[position].getArtist());
		genre.setText(values[position].getGenre());
		fileName.setText(values[position].getFilenName());
		// change the icon for Windows and iPhone
		return rowView;
	}
}
