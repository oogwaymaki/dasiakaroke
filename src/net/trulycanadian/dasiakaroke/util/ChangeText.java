package net.trulycanadian.dasiakaroke.util;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

public class ChangeText {

	public static TextView changeText(TextView textToChange, String changeWord) {

		Spannable s = new SpannableString(textToChange.getText().toString());
		int start = textToChange.getText().toString().indexOf(changeWord);
		int end = start + changeWord.length();
		s.setSpan(new ForegroundColorSpan(0xFFFF0000), start, end,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		textToChange.setText(s);
		return textToChange;
	}
	
}
