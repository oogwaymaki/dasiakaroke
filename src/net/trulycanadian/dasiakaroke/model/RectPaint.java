package net.trulycanadian.dasiakaroke.model;

import android.graphics.Paint;
import android.graphics.RectF;

public class RectPaint {

	public RectF getRect() {
		return rect;
	}
	public void setRect(RectF rect) {
		this.rect = rect;
	}
	public Paint getPaint() {
		return paint;
	}
	public void setPaint(Paint paint) {
		this.paint = paint;
	}
	RectF rect = new RectF();
	Paint paint = new Paint();;
}
