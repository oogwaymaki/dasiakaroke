package net.trulycanadian.dasiakaroke.model;

public class SongSummary {

	public String getSongName() {
		return songName;
	}

	public void setSongName(String songName) {
		this.songName = songName;
	}

	public String getAlbumName() {
		return albumName;
	}

	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getTrackNo() {
		return trackNo;
	}

	public void setTrackNo(String trackNo) {
		this.trackNo = trackNo;
	}

	private String songName;
	private String albumName;
	private String genre;
	private String artist;
	private String trackNo;
	private String filenName;

	public String getFilenName() {
		return filenName;
	}

	public void setFilenName(String filenName) {
		this.filenName = filenName;
	}

}
