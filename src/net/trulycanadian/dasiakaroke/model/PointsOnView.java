package net.trulycanadian.dasiakaroke.model;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import android.graphics.Paint;
import android.graphics.Point;

public class PointsOnView {

	/**
	 * 
	 */

	private HashMap<Point, SinglePoint> point = new HashMap<Point, SinglePoint>();
	// private HashMap<Point, SinglePoint> point = new HashMap<Point,
	// SinglePoint>();

	int x, y;
	boolean xt, yt = false;
    public boolean isClearScreen() {
        return clearScreen;
    }

    public void setClearScreen(boolean clearScreen) {
        this.clearScreen = clearScreen;
    }

    boolean clearScreen = false;
	public boolean containsXY(int x, int y) {
		if (point.containsKey(new Point(x, y))) {
			return true;
		} else {
			return false;
		}
	}

	public Map<Point, SinglePoint> returnRawHashMap() {
		return point;
	}

	public int getColor(int x, int y) {

		return point.get(new Point(x, y)).getPaint().getColor();
	}

	public byte getRawColor(int x, int y) {
		return point.get(new Point(x, y)).getColorRaw();
	}

	public Paint getPaint(int x, int y) {
		return point.get(new Point(x, y)).getPaint();
	}

	public void putAll(HashMap<Point, SinglePoint> pointsAdd) {

		point.putAll(pointsAdd);
	}

	public void put(SinglePoint singlePoint) {
		point.put(new Point(singlePoint.getX(), singlePoint.getY()),
				singlePoint);
	}




}
