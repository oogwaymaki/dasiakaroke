package net.trulycanadian.dasiakaroke.model;

import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;

import java.io.Serializable;

public class SinglePoint  {


    public SinglePoint()
    {
    }
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Paint getPaint() {
		return paint;
	}

	public void setPaint(Paint  paint) {
		this.paint = paint;
	}

	Paint paint = new Paint();

	int x;
	int y;
    byte colorRaw;

	public byte getColorRaw() {
		return colorRaw;
	}

	public void setColorRaw(byte colorRaw) {
		this.colorRaw = colorRaw;
	}
}
