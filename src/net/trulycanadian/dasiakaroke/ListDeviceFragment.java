package net.trulycanadian.dasiakaroke;

import java.io.File;
import java.util.ArrayList;

import net.trulycanadian.adapter.SongListAdapter;
import net.trulycanadian.dasiakaroke.cdg.service.DlnaService;
import net.trulycanadian.dasiakaroke.dlna.BrowseRegistryListener;
import net.trulycanadian.dasiakaroke.file.ScanSdCard;
import net.trulycanadian.dasiakaroke.model.SongSummary;

import org.teleal.cling.android.AndroidUpnpService;
import org.teleal.cling.model.meta.Device;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class ListDeviceFragment extends Fragment {

	private BrowseRegistryListener registryListener = new BrowseRegistryListener();
	Device myDevice = null;
	DlnaService service = new DlnaService();
	MainActivity activity = MainActivity.instance();
	private AndroidUpnpService upnpService;
	private ServiceConnection serviceConnection = new ServiceConnection() {

		public void onServiceConnected(ComponentName className, IBinder service) {

			upnpService = (AndroidUpnpService) service;
			for (Device device : upnpService.getRegistry().getDevices()) {
				registryListener.deviceAdded(device);
			}

			// Getting ready for future device advertisements
			upnpService.getRegistry().addListener(registryListener);

			registryListener.setUpnpservice(upnpService);
			upnpService.getControlPoint().search();
		}

		public void onServiceDisconnected(ComponentName className) {
			upnpService = null;
		}
	};

	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		MainActivity activity = (MainActivity) getActivity();
		activity.getApplicationContext().bindService(
				new Intent(getActivity(), DlnaService.class),
				serviceConnection, Context.BIND_AUTO_CREATE);
		activity.setDeviceNames(registryListener.getFileSystem());

	}

	public void refresh() {

	}

	public void onStart() {
		super.onStart();
		/*
		 * if (MainActivity.instance().getDeviceName()
		 * .contains("Local File System")) {
		 * 
		 * } else { for (Device device : registryListener.getDevices()) {
		 * Log.v("test", device.getDetails().getFriendlyName());
		 * 
		 * if (device.getDetails().getFriendlyName()
		 * .contains(MainActivity.instance().getDeviceName())) { myDevice =
		 * device; break; } }
		 * 
		 * try { } catch (Exception ex) { Log.v("Exception", ex.toString()); }
		 * 
		 * }
		 */
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		// This layout contains your list view
		View view = inflater.inflate(R.layout.fragment_main_dummy, container,
				false);

		// now you must initialize your list view
		ListView listview = (ListView) view.findViewById(R.id.listview);

		// To have custom list view use this : you must define
		// CustomeAdapter class
		// listview.setadapter(new CustomeAdapter(getActivity()));
		// getActivty is used instead of Context

		ArrayList<File> files = null;
		ScanSdCard walkdir = new ScanSdCard();
		walkdir.walkdir(new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/cdgmp3/"));
		files = walkdir.getFiles();
		SongSummary array[] = new SongSummary[files.size()];
		int i = 0;

		for (File file : files) {
			MediaMetadataRetriever mmr = new MediaMetadataRetriever();
			mmr.setDataSource(file.getAbsolutePath().replace(".cdg", ".mp3"));
			SongSummary tmp = new SongSummary();
			tmp.setAlbumName(mmr
					.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM));
			tmp.setArtist(mmr
					.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
			tmp.setGenre(mmr
					.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE));
			tmp.setSongName(mmr
					.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
			tmp.setFilenName(file.getAbsolutePath());
			Log.v("TMP", tmp.getFilenName());
			array[i++] = tmp;
			tmp = null;
		}
		SongListAdapter adapter = new SongListAdapter(getActivity(), array);

		listview.setAdapter(adapter);
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				TextView fileName = (TextView) view.findViewById(R.id.fileName);
				String fileNameString = fileName.getText().toString();
				activity.callService(fileNameString);
				// ListView Clicked item value

			}

		});
		return view;
	}
}
