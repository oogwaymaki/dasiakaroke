package net.trulycanadian.dasiakaroke;

import java.util.ArrayList;

import net.trulycanadian.dasiakaroke.util.SystemUiHider;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class ChangeFileInput extends Activity {
	ListView listView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
		setContentView(R.layout.activity_change_file_input);

		// Get ListView object from xml
		listView = (ListView) findViewById(R.id.listViewFileSystem);

		// Defined Array values to show in ListView
		ArrayList<String> array = intent.getStringArrayListExtra("dlna");
		array.add("FileSystem");

		// Define a new Adapter
		// First parameter - Context
		// Second parameter - Layout for the row
		// Third parameter - ID of the TextView to which the data is written
		// Forth - the Array of data

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, android.R.id.text1, array);

		// Assign adapter to ListView
		listView.setAdapter(adapter);

		// ListView Item Click Listener
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				MainActivity activity = MainActivity.instance();
				// ListView Clicked item value
				TextView textView = (TextView) view
						.findViewById(android.R.id.text1);
				Log.v("ERROR", textView.getText().toString());
				activity.setDeviceName(textView.getText().toString());
			}

		});
	}
}
