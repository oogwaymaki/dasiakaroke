package net.trulycanadian.dasiakaroke.dlna;

import java.util.ArrayList;

import org.teleal.cling.android.AndroidUpnpService;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.model.action.ActionArgumentValue;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.LocalDevice;
import org.teleal.cling.model.meta.RemoteDevice;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.types.ServiceId;
import org.teleal.cling.model.types.UDAServiceId;
import org.teleal.cling.registry.DefaultRegistryListener;
import org.teleal.cling.registry.Registry;
import org.teleal.cling.support.contentdirectory.callback.Browse;
import org.teleal.cling.support.model.BrowseFlag;
import org.teleal.cling.support.model.DIDLContent;
import org.teleal.cling.support.model.DIDLObject.Property;
import org.teleal.cling.support.model.Res;
import org.teleal.cling.support.model.container.Container;
import org.teleal.cling.support.model.container.StorageFolder;
import org.teleal.cling.support.model.item.Item;
import org.teleal.cling.support.model.item.MusicTrack;

import android.util.Log;

public class BrowseRegistryListener extends DefaultRegistryListener {

	public AndroidUpnpService getUpnpservice() {
		return upnpservice;
	}

	public void setUpnpservice(AndroidUpnpService upnpservice) {
		this.upnpservice = upnpservice;
	}

	private AndroidUpnpService upnpservice;
	ArrayList<Device> devices = new ArrayList<Device>();

	public ArrayList<Device> getDevices() {
		return devices;
	}

	public ArrayList<String> getFileSystem() {
		return fileSystem;
	}

	ArrayList<String> fileSystem = new ArrayList<String>();

	@Override
	public void remoteDeviceDiscoveryStarted(Registry registry,
			RemoteDevice device) {
		deviceAdded(device);
	}

	@Override
	public void remoteDeviceDiscoveryFailed(Registry registry,
			final RemoteDevice device, final Exception ex) {
		new Thread(new Runnable() {
			public void run() {
				Log.v("FAILED",
						"Discovery failed of '"
								+ device.getDisplayString()
								+ "': "
								+ (ex != null ? ex.toString()
										: "Couldn't retrieve device/service descriptors"));
			}

		}).start();

		deviceRemoved(device);
	}

	@Override
	public void remoteDeviceAdded(Registry registry, RemoteDevice device) {
		deviceAdded(device);
	}

	@Override
	public void remoteDeviceRemoved(Registry registry, RemoteDevice device) {
		deviceRemoved(device);
	}

	@Override
	public void localDeviceAdded(Registry registry, LocalDevice device) {
	}

	@Override
	public void localDeviceRemoved(Registry registry, LocalDevice device) {
		deviceRemoved(device);
	}

	public void deviceAdded(final Device device) {

		new Thread(new Runnable() {
			public void run() {
				Log.v("SERVICE TYPE: ", device.getType().getDisplayString());
				if (device.getType().getType().contains(("MediaServer"))) {
					fileSystem.add(device.getDetails().getFriendlyName());
					devices.add(device);

					Log.v("SERVICE TYPE: ", device.getType().getType());
					Log.v("Logging device found: ", device.getDisplayString());
//					ServiceId serviceId = new UDAServiceId("ContentDirectory");
//					Service service = device.findService(serviceId);
//					if (service.getAction("Browse") != null) {
//						ActionCallback callback = new Browse(service, "1$4",
//								BrowseFlag.DIRECT_CHILDREN) {
//
//							@Override
//							public void received(
//									ActionInvocation actionInvocation,
//									DIDLContent didl) {
//
//								ActionArgumentValue[] output = actionInvocation
//										.getOutput();
//								for (ActionArgumentValue value : actionInvocation
//										.getOutput()) {
//									Log.v("OUTPUT", value.getValue().toString());
//
//									// Something wasn't right...
//								}
//								Log.v("ITEM",
//										String.valueOf(didl.getItems().size()));
//								for (Item item : didl.getItems()) {
//									MusicTrack track = (MusicTrack) item;
//
//									for (Res res : item.getResources()) {
//			//							Log.v("RESOURCE", res.getValue());
//									}
//
//								}
//								Log.v("RECEIVED", String.valueOf(didl
//										.getContainers().size()));
//								for (Container container : didl.getContainers()) {
//									StorageFolder storage = (StorageFolder) container;
//									Log.v("STORAGE",
//											String.valueOf(storage.getId()
//													+ " " + storage.getTitle()));
//									for (Property property : storage
//											.getProperties()) {
//										Log.v("PROPERTY",
//												property.getDescriptorName());
//									}
//									Log.v("CONTAINER", container.getClass()
//											.toString());
//								}
//							}
//
//							@Override
//							public void updateStatus(Status status) {
//								// Called before and after loading the DIDL
//								// content
//							}
//
//							@Override
//							public void failure(ActionInvocation invocation,
//									UpnpResponse operation, String defaultMsg) {
//								Log.v("ERROR", defaultMsg);
//								// Something wasn't right...
//							}
//						};
//						if (callback != null)
//							upnpservice.getControlPoint().execute(callback);
//					}
					// try {
					//
					// ServiceId serviceId = new UDAServiceId(
					// "ContentDirectory");
					// Service service1 = device.findService(serviceId);
					// Action action = null;
					// for (Action action2 : service1.getActions()) {
					// Log.v("TEST", action2.toString());
					// if (action2.getName().equals("Browse")) {
					// System.out.println("GOt here");
					// action = action2;
					// break;
					// }
					// }
					//
					// ActionInvocation setTargetInvocation = new
					// ActionInvocation(
					// action);
					// setTargetInvocation.setInput("ObjectID", "0");
					// setTargetInvocation.setInput("BrowseFlag",
					// "BrowseDirectChildren");
					// setTargetInvocation.setInput("Filter", "*");
					// setTargetInvocation.setInput("StartingIndex", "0");
					// setTargetInvocation.setInput("RequestedCount", "100");
					// setTargetInvocation.setInput("SortCriteria", "");
					// System.out.println("blah");
					// ActionCallback setTargetCallback = new ActionCallback(
					// setTargetInvocation) {
					//
					// @Override
					// public void success(ActionInvocation invocation) {
					// ActionArgumentValue[] output = invocation
					// .getOutput();
					// Log.v("OUTPUT", String.valueOf(output.length));
					// for (ActionArgumentValue value : invocation
					// .getOutput()) {
					// Log.v("OUTPUT", value.getValue().toString());
					//
					// }
					// }
					//
					// @Override
					// public void failure(ActionInvocation invocation,
					// UpnpResponse operation, String defaultMsg) {
					// Log.v("ERROR", defaultMsg);
					// }
					// };
					// upnpservice.getControlPoint()
					// .execute(setTargetCallback);
					// } catch (Exception ex) {
					// Log.v("Exception", ex.toString());
					// }
				}
			}
		}).start();
	}

	public void deviceRemoved(final Device device) {
		fileSystem.remove(device);
	}
}
