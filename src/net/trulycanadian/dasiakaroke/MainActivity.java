package net.trulycanadian.dasiakaroke;

import java.util.ArrayList;
import java.util.Locale;

import net.trulycanadian.adapter.SongListAdapter;
import net.trulycanadian.dasiakaroke.cdg.service.CDGService;
import net.trulycanadian.dasiakaroke.model.SongSummary;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class MainActivity extends FragmentActivity {
	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public static float scale2;;
	public static float density;

	String deviceName = "Local File System";

	ArrayList<String> deviceNames = null;

	Bundle userBundle = new Bundle();

	public void setDeviceNames(ArrayList<String> deviceNames) {
		this.deviceNames = deviceNames;
	}

	public static MainActivity activity;
	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	public static MainActivity instance() {
		return activity;
	}

	public void callService(String filename) {
		Intent msgIntent = new Intent(this, CDGService.class);

		msgIntent.putExtra(CDGService.SongMP3, filename);
		startService(msgIntent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activity = this;
		setContentView(R.layout.activity_main);
		float scale = getResources().getDisplayMetrics().density;
		scale2 = scale;
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		try

		{

		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("test");
		}

	}

	public Bundle getUserBundle() {
		return userBundle;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			Fragment fragment = new ListDeviceFragment();
			Bundle args = new Bundle();
			args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position + 1);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			}
			return null;
		}
	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class DummySectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";

		public DummySectionFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// Inflate the layout for this fragment
			// This layout contains your list view
			View view = inflater.inflate(R.layout.fragment_main_dummy,
					container, false);

			// now you must initialize your list view
			ListView listview = (ListView) view.findViewById(R.id.listview);

			// To have custom list view use this : you must define
			// CustomeAdapter class
			// listview.setadapter(new CustomeAdapter(getActivity()));
			// getActivty is used instead of Context

			SongSummary[] array = new SongSummary[50];

			for (int i = 0; i < 50; i++) {
				SongSummary tmp = new SongSummary();
				tmp.setAlbumName("Album " + i);
				tmp.setArtist("Artist " + i);
				tmp.setGenre("Pop ");
				tmp.setSongName("This is a cool song " + i);
				array[i] = tmp;
				tmp = null;
			}
			SongListAdapter adapter = new SongListAdapter(getActivity(), array);

			listview.setAdapter(adapter);

			return view;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.action_settings:
			Intent intent = new Intent(this, ChangeFileInput.class);
			intent.putStringArrayListExtra("dlna", deviceNames);
			startActivity(intent);

			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	static public float convertPxtoDip(float pixel) {

		int dips = (int) ((pixel / scale2) + 0.5f);
		return dips;
	}
}
