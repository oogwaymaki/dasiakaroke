package net.trulycanadian.dasiakaroke.file;

import java.io.File;
import java.util.ArrayList;

import android.util.Log;

public class ScanSdCard {
	private ArrayList<File> files = new ArrayList<File>();

	public ArrayList<File> getFiles() {
		return files;
	}

	public void setFiles(ArrayList<File> files) {
		this.files = files;
	}

	public void walkdir(File dir) {
		String cdgPattern = "cdg";
		File[] listFile = dir.listFiles();

		if (listFile != null) {

			for (int i = 0; i < listFile.length; i++) {
				if (listFile[i].isDirectory()) {
					walkdir(listFile[i]);
				} else {
					if (listFile[i].getName().endsWith(cdgPattern)) {
						files.add(listFile[i]);
					}
				}
			}

		}
	}
}
