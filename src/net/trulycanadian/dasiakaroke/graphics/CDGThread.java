package net.trulycanadian.dasiakaroke.graphics;


import net.trulycanadian.dasiakaroke.model.PointsOnView;
import net.trulycanadian.dasiakaroke.model.SinglePoint;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.view.SurfaceHolder;

import java.util.Map;

public class CDGThread extends HandlerThread implements Handler.Callback {
    boolean mRun;
    Bitmap backBitMapBuffer;
    Canvas backCanvasBuffer;

    static CDGThread instance;

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    Handler handler = new Handler();

    public static CDGThread getInstance() {
        return instance;

    }
;
    SurfaceHolder surfaceHolder;
    CDGSurfaceView msurfacePanel;
    Context context;

    boolean clear;

    public CDGThread(SurfaceHolder sholder, Context ctx, CDGSurfaceView spanel)

    {
        super("DrawThread");
        CDGThread.instance = this;
        surfaceHolder = sholder;
        msurfacePanel = spanel;
        context = ctx;

        mRun = false;
    }

    public void setRunning(boolean bRun)

    {

        mRun = bRun;

    }

    @Override
    protected void onLooperPrepared() {
        handler = new Handler(getLooper(), this);
    }

    public void doDraw(PointsOnView pov)

    {
        try {
                 Paint paint = new Paint();
                 paint.reset();
                 paint.setAntiAlias(true);
                 paint.setFilterBitmap(true);

                if (backBitMapBuffer == null) {
                    this.backBitMapBuffer = Bitmap.createBitmap(299, 215, Bitmap.Config.ARGB_8888);
                    backCanvasBuffer = new Canvas();
                }
                backCanvasBuffer.setBitmap(backBitMapBuffer);

                    Map<Point, SinglePoint> map = pov.returnRawHashMap();
                    if (map.size() > 0) {
                        for (Map.Entry<Point, SinglePoint> entry :
                                map.entrySet()) {
                            backCanvasBuffer.drawPoint(entry.getValue().getX(), entry
                                    .getValue().getY(), entry.getValue().getPaint());
                        }
                    }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    @Override
    public boolean handleMessage(Message msg) {
        try {

            Bitmap bitmap =  Bitmap.createBitmap(299,215,Bitmap.Config.ARGB_8888);

                       Canvas onScreenCanvas = null;
                       if (msg.obj instanceof PointsOnView) {

                           PointsOnView pov = (PointsOnView) msg.obj;
//                           if (pov.isClearScreen())
//                              msurfacePanel.setBackgroundColor(pov.getColor(100, 100));
                           doDraw(pov);

                           }
                       onScreenCanvas = surfaceHolder.lockCanvas();
                    if (onScreenCanvas != null) {
                        onScreenCanvas.drawBitmap(backBitMapBuffer, 0, 0, null);
                        surfaceHolder.unlockCanvasAndPost(onScreenCanvas);
                    }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return true;


    }
}
