package net.trulycanadian.dasiakaroke.graphics;

import java.util.HashMap;
import java.util.Map;

import net.trulycanadian.dasiakaroke.MainActivity;
import net.trulycanadian.dasiakaroke.model.PointsOnView;
import net.trulycanadian.dasiakaroke.model.SinglePoint;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class CDGSurfaceView extends GLSurfaceView implements
        SurfaceHolder.Callback {


    private Bitmap prevBitMap;
    public static CDGSurfaceView surfaceView;
    public static CDGSurfaceView instance() {
        return surfaceView;
    }

    public static CDGSurfaceView getSurfaceView() {
        return surfaceView;
    }

    public static void setSurfaceView(CDGSurfaceView surfaceView) {
        CDGSurfaceView.surfaceView = surfaceView;
    }

    SurfaceHolder holder = getHolder();
    CDGThread thread;
    MainActivity context;


    public void draw(Bitmap bitmap)
    {

            Canvas canvas = new Canvas(bitmap);
            canvas.drawBitmap(bitmap,0,0,null);
            draw(canvas);
    }
    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {


    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        thread = new CDGThread(holder, context, this);
        holder.setFixedSize(299, 215);
        thread.setRunning(true);

        thread.start();

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

        thread.setRunning(false);

        boolean retry = true;

        while (retry)

        {

            try

            {

                thread.join();

                retry = false;

            } catch (Exception e)

            {

                Log.v("Exception Occured", e.getMessage());

            }

        }

    }



    public CDGSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = (MainActivity) context;
        CDGSurfaceView.surfaceView = this;
        System.out.println("inside here 1");
        holder.addCallback(this);
    }


}
