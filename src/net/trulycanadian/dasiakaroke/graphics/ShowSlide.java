package net.trulycanadian.dasiakaroke.graphics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ShowSlide extends ImageView {

	public Bitmap getImageView() {
		return imageView;
	}

	public void setImageView(Bitmap imageView) {
		this.imageView = imageView;
	}

	Bitmap imageView;

	public ShowSlide(Context context) {
		super(context);
	}

	public ShowSlide(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ShowSlide(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public void doAnimation()
	{
		
	}
	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawBitmap(imageView, 0.0f, 0.0f, (Paint) null);
		invalidate();
	}

}
