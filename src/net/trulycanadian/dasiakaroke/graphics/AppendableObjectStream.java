package net.trulycanadian.dasiakaroke.graphics;

import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.IOException;
/**
 * Created by davegermiquet on 2014-09-01.
 */
public class AppendableObjectStream extends ObjectOutputStream{

    public AppendableObjectStream(OutputStream out)  throws IOException{
           super(out);
    }

    @Override
    protected void writeStreamHeader() throws IOException {
        // do not write a header, but reset:
        // this line added after another question
        // showed a problem with the original
        reset();
    }

}
