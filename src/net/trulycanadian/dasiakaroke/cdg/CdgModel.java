package net.trulycanadian.dasiakaroke.cdg;

import android.graphics.Color;

public class CdgModel {
	
	public byte getCommand() {
		return command;
	}
	public void setCommand(byte command) {
		this.command = command;
	}
	public byte getInstruction() {
		return instruction;
	}
	public void setInstruction(byte instruction) {
		this.instruction = instruction;
	}
	public byte[] getParityP() {
		return parityP;
	}
	public void setParityP(byte[] parityP) {
		this.parityP = parityP;
	}
	public byte[] getData() {
		return data;
	}
	public void setData(byte[] data) {
		this.data = data;
	}
	public byte[] getParityQ() {
		return parityQ;
	}
	public void setParityQ(byte[] parityQ) {
		this.parityQ = parityQ;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public static Color[] getColormap() {
		return colormap;
	}
	public static void setColormap(Color[] colormap) {
		CdgModel.colormap = colormap;
	}
	private byte command;
	private byte instruction;
	private byte[] parityP = new byte[2];
	private byte[] data = new byte[16];
	private byte[] parityQ = new byte[4];
	private int offset;

	private static Color[] colormap = new Color[16];
	// All the constants used for the cdgInstruction
	// Set the screen to a particular color.
	public static byte CDG_COMMAND = 0x09;
	public static byte CDG_MASK = 0x03F;
	public final static byte CDG_MEMORY_PRESET = 1;
	// Set the border of the screen to a particular color.
	public final static byte CDG_BORDER_PRESET = 2;
	// Load a 12 x 6, 2 color tile and display it normally.
	public final static byte CDG_TILE_NORMAL = 6;
	// Scroll the image, filling in the new area with a color.
	public final static byte CDG_SCROLL_PRESET = 20;
	// Scroll the image, rotating the bits back around.
	public final static byte CDG_SCROLL_COPY = 24;
	// Define a specific color as being transparent.
	public final static byte CDG_DEF_TRANS_COLOR = 28;
	// Load in the lower 8 entries of the color table.
	public final static byte CDG_LOAD_COL_TABLE_LOW = 30;
	// Load in the upper 8 entries of the color table.
	public final static byte CDG_LOAD_COL_TABLE_HIGH = 31;
	// Load a 12 x 6, 2 color tile and display it using the XOR method.
	public final static byte CDG_TILE_XOR = 38;
}
