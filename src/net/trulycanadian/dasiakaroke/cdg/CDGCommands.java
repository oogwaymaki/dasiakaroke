package net.trulycanadian.dasiakaroke.cdg;

import net.trulycanadian.dasiakaroke.graphics.CDGSurfaceView;
import net.trulycanadian.dasiakaroke.model.PointsOnView;
import net.trulycanadian.dasiakaroke.model.SinglePoint;

public class CDGCommands {
	// static int WIDTH = MainActivity.convertPxtoDip(300);
	// static int HEIGHT = MainActivity.convertPxtoDip(216);
	public static int WIDTH = 299;
	public static int HEIGHT = 215;

	public static PointsOnView memPresetCommand(byte[] data,PointsOnView pointsToAdd) {
        // data is the 16 bytes array of the cdg chunk.
        PointsOnView pointsChange = null;
            byte color;

            byte repeat;
            // For these, only the lower 4 bits are used
            color = (byte) (data[0] & 0x0F);
            repeat = (byte) (data[1] & 0x0F);

            if (repeat == 0) {
                pointsChange = new PointsOnView();
                System.out.println("Received a clear screen command");
                for (int y = 0; y < HEIGHT; y++) {
                    for (int x = 0; x < WIDTH; x++) {
                        SinglePoint singlePoint = null;
                        singlePoint = new SinglePoint();
                        singlePoint.setX(x);
                        singlePoint.setY(y);
                        singlePoint.getPaint().setColor(
                                CDGImageCreator.colormap[color]);
                        singlePoint.setColorRaw(color);
                        pointsToAdd.put(singlePoint);
                        pointsChange.put(singlePoint);

                    }
                }
                pointsChange.setClearScreen(true);
            }
        return pointsChange;
        }

	// byte []filler = new byte[15];
	public static PointsOnView drawBorder(byte[] data,PointsOnView pointsToAdd) {
		byte color;
        System.out.println("got border");
        PointsOnView  pointsToChange = null;

		color = (byte) (data[0] & 0x0F);

	/*	for (int x = 6; x < 294; x++) {
			for (int y = 12; y < 204; y++) {
				SinglePoint singlePoint = new SinglePoint();
				singlePoint.setX(x);
				singlePoint.setY(y);
				singlePoint.getPaint()
						.setColor(CDGImageCreator.colormap[color]);
				singlePoint.setColorRaw(color);
				pointsToAdd.put(singlePoint);
			}
		} */
        pointsToChange = new PointsOnView();
        for (int y = 0 ; y < HEIGHT;y++)
        for (int x = 0 ; x < 5; x++)
        {
            SinglePoint singlePoint = new SinglePoint();
            singlePoint.setX(x);
            singlePoint.setY(y);
            singlePoint.getPaint().setColor(CDGImageCreator.colormap[color]);
            singlePoint.setColorRaw(color);
            pointsToAdd.put(singlePoint);
            pointsToChange.put(singlePoint);
        }
        for (int y=0;y<HEIGHT;y++)
        for (int x = 294; x < WIDTH ; x++) {
            SinglePoint singlePoint = new SinglePoint();
            singlePoint.setX(x);
            singlePoint.setY(y);
            singlePoint.getPaint().setColor(CDGImageCreator.colormap[color]);
            singlePoint.setColorRaw(color);
            pointsToAdd.put(singlePoint);
            pointsToChange.put(singlePoint);
        }
        for (int x=0; x < WIDTH ; x++)
		for (int y = 0; y < 12; y++) {
			SinglePoint singlePoint = new SinglePoint();
			singlePoint.setX(x);
			singlePoint.setY(y);
			singlePoint.getPaint().setColor(CDGImageCreator.colormap[color]);
			singlePoint.setColorRaw(color);
			pointsToAdd.put(singlePoint);
            pointsToChange.put(singlePoint);
		}
        for (int x=0;x<WIDTH;x++)
		for (int y = 204 ; y < HEIGHT; y++) {
			SinglePoint singlePoint = new SinglePoint();
			singlePoint.setX(x);
			singlePoint.setY(y);
			singlePoint.getPaint().setColor(CDGImageCreator.colormap[color]);
			singlePoint.setColorRaw(color);
			pointsToAdd.put(singlePoint);
            pointsToChange.put(singlePoint);

	    }
        return pointsToChange;

	}
}
