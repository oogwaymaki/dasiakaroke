package net.trulycanadian.dasiakaroke.cdg;

public class CDGDecoder {
	private CdgModel cdgData;

	public CdgModel readDataIntoPacket(byte[] data, int offset) {

		// Initialize attributes from the 24 bytes data array
		// For the command and instruction, only the lower 6 bits are relevant,
		// that's why we and them with ox3f
		byte[] parityP = new byte[2];
		byte[] parityQ = new byte[4];
		byte[] dataInput = new byte[16];

		cdgData = new CdgModel();
		cdgData.setCommand((byte) (data[offset + 0] & CdgModel.CDG_MASK));
		cdgData.setInstruction((byte) (data[offset + 1] & CdgModel.CDG_MASK));

		parityQ[0] = (byte) data[offset + 2 ];
		parityQ[1] = (byte) data[offset + 3 ];

		cdgData.setParityP(parityQ);

		// The 16 bytes data chunk
		for (int i = 0; i < 16; i++) {
			dataInput[i] = data[offset + 4 + i];
		}
		cdgData.setData(dataInput);
		parityP[0] = data[offset + 20];
		parityP[0] = data[offset + 21];
		parityP[0] = data[offset + 22];
		parityP[0] = data[offset + 23];
		cdgData.setParityQ(parityP);
		return cdgData;
	}

	
}
