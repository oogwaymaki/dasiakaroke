package net.trulycanadian.dasiakaroke.cdg;

import net.trulycanadian.dasiakaroke.model.PointsOnView;
import net.trulycanadian.dasiakaroke.model.SinglePoint;

public class CDGWriteFont {
	public static final int WIDTH = 299;

	public static final int HEIGHT = 215;
	private static byte color0;

	private static byte color1;

	private static byte[] tilePixels = new byte[12];

	public static PointsOnView drawTile(byte[] data, boolean XORMode,
			PointsOnView pointsToAdd) {
           PointsOnView pointsToChange = null;

		color0 = (byte) (data[0] & 0x0F);
		color1 = (byte) (data[1] & 0x0F);
		byte row = (byte) (data[2] & 0x1F);
		byte column = (byte) (data[3] & 0x3F);
        if (row > HEIGHT - 12)
            System.out.println("OH OH");
        if (column > WIDTH - 6)
            System.out.println("OH OH");
		for (int i = 0; i < 12; i++) {
			tilePixels[i] = (byte) (data[4 + i] & 0x3F);
		}
        pointsToChange = new PointsOnView();
		int x = column * 6;
		int y = row * 12;
		int five = 5;
		int y3 = 0;
		for (int i = 0; i < tilePixels.length; i++) {
			for (int columnToChange = 0; columnToChange < 6; columnToChange++) {
				try {
					if ((tilePixels[i] & 0x1) == 0) {
						if (XORMode) {
							byte tmp = 0;
							int x2 = x + (five - columnToChange);
							int y2 = y3 + y;
							SinglePoint singlePoint = new SinglePoint();
							singlePoint.setX(x2);
							singlePoint.setY(y2);
                            tmp = pointsToAdd.getRawColor(x2,y2);
							tmp ^= color0;
							singlePoint.setColorRaw(tmp);
							singlePoint.getPaint().setColor(
									CDGImageCreator.colormap[tmp]);
							pointsToAdd.put(singlePoint);
                            pointsToChange.put(singlePoint);

						} else {
							int x2 = x + (five - columnToChange);
							int y2 = y3 + y;
							SinglePoint singlePoint = new SinglePoint();
							singlePoint.setX(x2);
							singlePoint.setY(y2);
							singlePoint.setColorRaw(color0);
							singlePoint.getPaint().setColor(
									CDGImageCreator.colormap[color0]);
							pointsToAdd.put(singlePoint);
                            pointsToChange.put(singlePoint);
						}
					} else {
						if (XORMode) {
							byte tmp = 0;

							int x2 = x + (five - columnToChange);
							int y2 = y3 + y;
							SinglePoint singlePoint = new SinglePoint();
							singlePoint.setX(x2);
							singlePoint.setY(y2);
                            tmp = pointsToAdd.getRawColor(x2,y2);
							tmp ^= color1;
							singlePoint.setColorRaw(tmp);
							singlePoint.getPaint().setColor(
									CDGImageCreator.colormap[tmp]);
							pointsToAdd.put(singlePoint);
                            pointsToChange.put(singlePoint);
						} else {
							int x2 = x + (five - columnToChange);
							int y2 = y3 + y;
							SinglePoint singlePoint = new SinglePoint();
							singlePoint.setX(x2);
							singlePoint.setY(y2);
                            singlePoint.setColorRaw(color1);
							singlePoint.getPaint().setColor(
									CDGImageCreator.colormap[color1]);
							pointsToAdd.put(singlePoint);
                            pointsToChange.put(singlePoint);

						}
					}

				} catch (ArrayIndexOutOfBoundsException ex) {
					ex.printStackTrace();
					System.out.println("Problem in reading tile block");
				}
				tilePixels[i] = (byte) (tilePixels[i] >> 1);
			}
			y = y + 1;
		}
        return pointsToChange;
	}
}
