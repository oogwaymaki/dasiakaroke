package net.trulycanadian.dasiakaroke.cdg;

import android.app.IntentService;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;

import net.trulycanadian.dasiakaroke.graphics.CDGSurfaceView;
import net.trulycanadian.dasiakaroke.graphics.CDGThread;
import net.trulycanadian.dasiakaroke.model.PointsOnView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PipedOutputStream;
import java.util.ArrayList;

public class CDGImageCreator {
	CDGSurfaceView layout = CDGSurfaceView.instance();
	//
	// class Сalculate extends TimerTask {
	//
	// @Override
	// public void run() {
	// if (commandsProcessed > 3000) {
	// layout.setPoints(points);
	// layout.setShouldDraw(true);
	// commandsProcessed = 0;
	// } else {
	// layout.setShouldDraw(false);
	// }
	// this.cancel();
	// }
	// }

	public static boolean instance = false;;
    PointsOnView pointsToChange = new PointsOnView();
	CdgModel cdgData[];
	int commandsProcessed = 0;
	int commandsProcessed2 = 0;
	int previousProccessed2 = 0;
	private long time2 = 0;
	private long time = 0;
	Bitmap bitmap;
	ArrayList<Long> line1 = new ArrayList<Long>();
	ArrayList<Long> line2 = new ArrayList<Long>();
	private boolean finished;
	public static int[] colormap = new int[16];
	private MediaPlayer player;

	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setMediaPlayer(MediaPlayer player) {

	}

	public synchronized void sendStream(PointsOnView pointsOnView) {
		try {
            CDGThread thread = CDGThread.getInstance();
            Handler handler = thread.getHandler();
            Message message = new Message();
            message.obj = null;
            message.obj = pointsOnView;
            handler.sendMessage(message);
		} catch (Exception e ) {
			try {

			} catch (Exception ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();

		} finally {
			try {
			} catch (Exception ex) {

			}
		}
	}

	public void displayGraphicOnSurfaceVIew() {

		instance = true;
		System.out.println("length " + cdgData.length);
		boolean flag = true;
		long startTime, startTimePlayer, endTimePlayer = 0;
		long endTime;

		int delay = player.getDuration() / 26;
		finished = false;
		long startTimeStart = System.currentTimeMillis();

		for (int i = 0; i < cdgData.length; i++) {
			if (cdgData[i] == null) {
				instance = false;
				finished = true;
				break;
			}
			try {
//                if ((cdgData[i].getInstruction() & CdgModel.CDG_MASK) == CdgModel.CDG_MEMORY_PRESET)
//                {
//                   PointsOnView change =  processCommand(cdgData[i]);
//                        sendStream(change);
//
//                }
//                else  if ((cdgData[i].getInstruction() & CdgModel.CDG_MASK)== CdgModel.CDG_BORDER_PRESET)
//                {
//                    PointsOnView change =  processCommand(cdgData[i]);
//                    sendStream(change);
//
//                }
//                else {
                   PointsOnView send = processCommand(cdgData[i]);
                   sendStream(send);
    //            }


			} catch (Exception ex) {
				ex.printStackTrace();

			}

		}
		long endTimeFinish = System.currentTimeMillis() - startTimeStart;
		System.out.println(endTimeFinish);
	}

	public CDGImageCreator(CdgModel cdgData[], final IntentService service,
			MediaPlayer player) {

		this.player = player;
		this.cdgData = cdgData;
		System.out.println("go there");
	}

	public PointsOnView processCommand(CdgModel cdgData) {
             PointsOnView pointsChange = null;
		try {
			switch (cdgData.getInstruction() & CdgModel.CDG_MASK) {
			case CdgModel.CDG_MEMORY_PRESET:
				pointsChange = CDGCommands.memPresetCommand(cdgData.getData(),pointsToChange);
           break;
			case CdgModel.CDG_BORDER_PRESET:
				 pointsChange = CDGCommands.drawBorder(cdgData.getData(),pointsToChange);
            break;
			case CdgModel.CDG_TILE_NORMAL:
				pointsChange = CDGWriteFont.drawTile(cdgData.getData(), false,
						pointsToChange);
            break;
			case CdgModel.CDG_TILE_XOR:
				pointsChange = CDGWriteFont.drawTile(cdgData.getData(), true,
						pointsToChange);
            break;
			case CdgModel.CDG_SCROLL_COPY:
				pointsChange = CDGScroll.CdgScrollCopy(cdgData.getData(),
						pointsToChange);
            break;
			case CdgModel.CDG_SCROLL_PRESET:
                pointsChange = CDGScroll.scrollPreset(cdgData.getData(),pointsToChange);
            break;
			case CdgModel.CDG_LOAD_COL_TABLE_LOW:
				CDGLoadColorTableLow
						.setColormap(cdgData.getData(), 0, colormap);
                pointsChange = new PointsOnView();
            break;
			case CdgModel.CDG_LOAD_COL_TABLE_HIGH:
				CDGLoadColorTableLow
						.setColormap(cdgData.getData(), 8, colormap);
                pointsChange = new PointsOnView();
            break;

			default:
                return new PointsOnView();
			}
	    return pointsChange;
        } catch (Exception ex) {
			ex.printStackTrace();
		}
        return null;
	}

	public byte[] serialize(Object obj) throws IOException {
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		ObjectOutputStream o = new ObjectOutputStream(b);
		o.writeObject(obj);
		return b.toByteArray();
	}
}
