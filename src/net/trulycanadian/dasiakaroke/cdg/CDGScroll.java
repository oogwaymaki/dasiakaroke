package net.trulycanadian.dasiakaroke.cdg;

import net.trulycanadian.dasiakaroke.model.PointsOnView;
import net.trulycanadian.dasiakaroke.model.SinglePoint;

public class CDGScroll {
	// public static final int WIDTH = MainActivity.convertPxtoDip(300);

	// public static final int HEIGHT = MainActivity.convertPxtoDip(216);

	public static int HEIGHT = 216;
	public static int WIDTH = 300;

	/**
	 * @return true if a scroll is done, false if only an offset is done
	 */
	public static PointsOnView CdgScrollCopy(byte data[],
			PointsOnView pointsToAdd) {
        System.out.println("GOT SCROLL");
        PointsOnView pointsToChange = new PointsOnView();
		byte hcmd;
		byte vcmd;
		byte hOffset;
		byte vOffset;

		int h = 0, v = 0;
		boolean ret = false;
		hcmd = (byte) ((data[1] & 0x30) >> 4);
		vcmd = (byte) ((data[2] & 0x30) >> 4);
		hOffset = (byte) ((data[1] & 0x7) % 6);
		vOffset = (byte) ((data[2] & 0xF) % 12);

		switch (hcmd) {
		case 0:
			h = hOffset;
			break;
		case 2: // left
			h = 6;
			ret = true;
			break;
		case 1: // right
			h = -6;
			ret = true;
			break;
		default:
			break;
		}
		int tmp;
		if (h > 0) {

			for (int x = 0; x < WIDTH; x++) {
				for (int y = 0; y < HEIGHT; y++) {
					tmp = pointsToAdd.getColor(x - h, y);
					SinglePoint singlePoint = new SinglePoint();
					singlePoint.setX(x - h);
					singlePoint.setY(y);
					singlePoint.getPaint().setColor(
							pointsToAdd.getColor(x, y));
					pointsToAdd.put(singlePoint);
                    pointsToChange.put(singlePoint);
					singlePoint = null;
					singlePoint = new SinglePoint();
					singlePoint.setX(x);
					singlePoint.setY(y);
					singlePoint.getPaint().setColor(tmp);
					pointsToAdd.put(singlePoint);
                    pointsToChange.put(singlePoint);
				}
			}
		} else {
			h *= -1;
			for (int x = 0; x < WIDTH; x++) {
				for (int y = 0; y < HEIGHT; y++) {
					tmp = pointsToAdd.getColor(x - h, y);
					SinglePoint singlePoint = new SinglePoint();
					singlePoint.setX(x - h);
					singlePoint.setY(y);
					singlePoint.getPaint().setColor(
							pointsToAdd.getColor(x, y));
					pointsToAdd.put(singlePoint);
                    pointsToChange.put(singlePoint);
					singlePoint = null;
					singlePoint = new SinglePoint();
					singlePoint.setX(x);
					singlePoint.setY(y);
					singlePoint.getPaint().setColor(tmp);
					pointsToAdd.put(singlePoint);
                    pointsToChange.put(singlePoint);
				}
			}
		}

		switch (vcmd) {
		case 0:
			v = vOffset;
			break;
		case 2: // up
			v = 12;
			ret = true;
			break;
		case 1: // down
			v = -12;
			ret = true;
			break;
		default:
			break;
		}

		if (v > 0) {

			for (int x = 0; x < WIDTH; x++) {
				for (int y = 0; y < HEIGHT; y++) {

					tmp = pointsToAdd.getColor(x, y - v);
					SinglePoint singlePoint = new SinglePoint();
					singlePoint.setX(x);
					singlePoint.setY(y - v);
					singlePoint.getPaint().setColor(
							pointsToAdd.getColor(x, y));
					pointsToAdd.put(singlePoint);
                    pointsToChange.put(singlePoint);
					singlePoint = null;
					singlePoint = new SinglePoint();
					singlePoint.setX(x);
					singlePoint.setY(y);
					singlePoint.getPaint().setColor(tmp);
					pointsToAdd.put(singlePoint);
                    pointsToChange.put(singlePoint);
				}
			}
		}

		else {
			v *= -1;
			for (int x = 0; x < WIDTH; x++) {
				for (int y = 0; y < HEIGHT; y++) {

					tmp = pointsToAdd.getColor(x, y - v);
					SinglePoint singlePoint = new SinglePoint();
					singlePoint.setX(x);
					singlePoint.setY(y - v);
					singlePoint.getPaint().setColor(
							pointsToAdd.getColor(x, y));
					pointsToAdd.put(singlePoint);
                    pointsToChange.put(singlePoint);
					singlePoint = null;
					singlePoint = new SinglePoint();
					singlePoint.setX(x);
					singlePoint.setY(y);
					singlePoint.getPaint().setColor(tmp);
					pointsToAdd.put(singlePoint);
                    pointsToChange.put(singlePoint);
				}
			}
		}
        return pointsToChange;
	}

	public static PointsOnView scrollPreset(byte data[],PointsOnView pointsToAdd) {
		byte color;
		byte horizentalcmd;
		byte vcmd;
		byte hOffset;
		byte vOffset;
		int h = 0, v = 0;
		boolean ret = false;
		PointsOnView pointsToChange = new PointsOnView();
		// Only 4 lower bits are used
		color = (byte) ((data[0] & 0x0F) + 2);
		// Only lower 6 bits are used
		horizentalcmd = (byte) ((data[1] & 0x30) >> 4);
		vcmd = (byte) ((data[2] & 0x30) >> 4);
		hOffset = (byte) ((data[1] & 0x7) % 6);
		vOffset = (byte) ((data[2] & 0xF) % 12);

		switch (horizentalcmd) {
		case 0:
			h = hOffset;
			break;
		case 2:
			h = 6;
			ret = true;
			break;
		case 1:
			h = -6;
			ret = true;
			break;
		default:
			break;
		}

		if (h > 0) {
			for (int x = 0; x < WIDTH; x++) {
				for (int y = 0; y < HEIGHT; y++) {
					SinglePoint singlePoint = new SinglePoint();
					singlePoint.setX(x - h);
					singlePoint.setY(y);
					singlePoint.getPaint().setColor(
							CDGImageCreator.colormap[color]);
					pointsToAdd.put(singlePoint);
                    pointsToChange.put(singlePoint);
				}

			}
		} else {
			h *= -1;
			for (int x = 0; x < WIDTH; x++) {
				for (int y = 0; y < HEIGHT; y++) {
					SinglePoint singlePoint = new SinglePoint();
					singlePoint.setX(x - h);
					singlePoint.setY(y);
					singlePoint.getPaint().setColor(
							CDGImageCreator.colormap[color]);
					pointsToAdd.put(singlePoint);
                    pointsToChange.put(singlePoint);
				}

			}
		}

		switch (vcmd) {
		case 0:
			v = vOffset;
			break;
		case 2: // up
			v = 12;
			ret = true;
			break;
		case 1: // down
			v = -12;
			ret = true;
			break;
		default:
			break;
		}

		if (v > 0) {
			for (int x = 0; x < WIDTH; x++) {
				for (int y = 0; y < HEIGHT; y++) {
					SinglePoint singlePoint = new SinglePoint();
					singlePoint.setX(x);
					singlePoint.setY(y - v);
					singlePoint.getPaint().setColor(
							CDGImageCreator.colormap[color]);
					pointsToAdd.put(singlePoint);
                    pointsToChange.put(singlePoint);
				}

			}

		} else {
			v *= -1;
			for (int x = 0; x < WIDTH; x++) {
				for (int y = 0; y < HEIGHT; y++) {
					SinglePoint singlePoint = new SinglePoint();
					singlePoint.setX(x);
					singlePoint.setY(y - v);
					singlePoint.getPaint().setColor(
							CDGImageCreator.colormap[color]);
					pointsToAdd.put(singlePoint);
                    pointsToChange.put(singlePoint);
				}

			}
		}
        return pointsToChange;
	}
}
