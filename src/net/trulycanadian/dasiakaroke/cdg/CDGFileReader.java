package net.trulycanadian.dasiakaroke.cdg;

import java.io.IOException;
import java.io.InputStream;

public class CDGFileReader {

	CdgModel[] cdgData;

	public CdgModel[] getCdgData() {
		return cdgData;
	}

	public void setCdgData(CdgModel[] cdgData) {
		this.cdgData = cdgData;
	}

	public CDGFileReader(InputStream is) {
		try {
			readFileAsAByteArray(is);
		} catch (Exception ex) {
		}
	}

	private void readFileAsAByteArray(InputStream is) throws IOException {
		InputStream in = is;
		int fileLength = is.available();
		int dataPackets = fileLength / 24;
		cdgData = new CdgModel[dataPackets];
		byte[] rawdata = new byte[fileLength];
		in.read(rawdata);
		int nb = 0;
		for (int i = 0; i < fileLength; i += 24) {

			CDGDecoder decoder = new CDGDecoder();
			CdgModel dataChunk = decoder.readDataIntoPacket(rawdata, i);
			if (dataChunk.getCommand() == CdgModel.CDG_COMMAND) {
				cdgData[nb++] = dataChunk;
			}

		}
		in.close();
	}
}
