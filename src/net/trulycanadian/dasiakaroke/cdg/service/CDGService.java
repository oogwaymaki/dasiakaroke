package net.trulycanadian.dasiakaroke.cdg.service;

import java.io.FileInputStream;

import net.trulycanadian.dasiakaroke.cdg.CDGFileReader;
import net.trulycanadian.dasiakaroke.cdg.CDGImageCreator;
import net.trulycanadian.dasiakaroke.cdg.CdgModel;
import android.app.IntentService;
import android.content.Intent;
import android.media.MediaPlayer;
import android.util.Log;

public class CDGService extends IntentService {
	public static final String SongMP3 = "net.trulycanadian.dasiakaroke.SONG";
	public static final String bitmap = "net.trulycanadian.dasiakaroke.BITMAP";
	public static final String rectangle = "net.trulycanadian.dasiakaroke.RECT";
	public static final String paintme = "net.trulycanadian.dasiakaroke.PAINT";
	MediaPlayer mMediaPlayer = new MediaPlayer();
	String fileName;

	@Override
	protected void onHandleIntent(Intent arg0) {

		try {

			fileName = arg0.getStringExtra(CDGService.SongMP3);
			Log.v("FILENAME", fileName);
			if (CDGImageCreator.instance == false) {

				playCdgText(fileName);
			}
		} catch (Exception ex) {
			Log.v("EXCEPTION", ex.toString());
		}
	}

	public CDGService() {
		super("CDGService");
	}

	public void onPrepared(MediaPlayer player) {
		try {

			Thread thread = new Thread() {
				@Override
				public void run() {

				}

			};

			thread.start();

			Log.v("CDG", fileName);

		} catch (Exception ex) {
			Log.d("Exception", ex.toString());
		}
	}

	public void playCdgText(String sample) {
		try {

			CDGFileReader fileReader = new CDGFileReader(new FileInputStream(
					sample));
			System.out.println(fileReader.getCdgData().length);
			@SuppressWarnings("unused")
			CdgModel[] model = fileReader.getCdgData();
			if (model != null) {
				mMediaPlayer.setDataSource(fileName.replace(".cdg", ".mp3"));
				mMediaPlayer.prepare();
				mMediaPlayer.start();
				CDGImageCreator creator = new CDGImageCreator(model, this,
						this.mMediaPlayer);
				System.out.println("got here");
				creator.displayGraphicOnSurfaceVIew();
			}
		} catch (Exception ex) {
			Log.v("Exception", ex.toString());
		}

	}
}
