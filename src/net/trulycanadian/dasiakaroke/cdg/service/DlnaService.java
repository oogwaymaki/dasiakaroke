package net.trulycanadian.dasiakaroke.cdg.service;

import org.teleal.cling.android.AndroidUpnpServiceConfiguration;
import org.teleal.cling.android.AndroidUpnpServiceImpl;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.model.action.ActionArgumentValue;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Action;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.types.ServiceId;
import org.teleal.cling.model.types.ServiceType;
import org.teleal.cling.model.types.UDAServiceId;
import org.teleal.cling.model.types.UDAServiceType;

import android.net.wifi.WifiManager;
import android.util.Log;

public class DlnaService extends AndroidUpnpServiceImpl {
	@Override
	protected AndroidUpnpServiceConfiguration createConfiguration(
			WifiManager wifiManager) {
		return new AndroidUpnpServiceConfiguration(wifiManager) {

			@Override
			public ServiceType[] getExclusiveServiceTypes() {
				return new ServiceType[] { new UDAServiceType(
						"ContentDirectory") };
			}

			@Override
			public int getRegistryMaintenanceIntervalMillis() {
				return 7000;
			}

		};
	}

	public void getSearchFields(Device myDevice) {

	}
}
