package net.trulycanadian.dasiakaroke.cdg;

import android.graphics.Color;

public class CDGLoadColorTableLow {

	public static boolean setColormap(byte[] data, int startIndex,
			int[] colormap) {
		byte lowByte, highByte;
		byte red, blue, green;

		// We have to parse 8 colors
		for (int i = 0; i < 16; i += 2) {
			highByte = data[i];
			highByte &= 0x3f;

			lowByte = data[i + 1];
			lowByte &= 0x3f;
			blue = (byte) (lowByte & 0xF);
			red = (byte) (highByte >> 2);
			green = (byte) (((highByte & 0x3) * 4) + (lowByte >> 4));
			setColormapElement(colormap, startIndex, i, (red * 10),
					(green * 10), (blue * 10));
		}
		return true;
	}

	public static void setColormapElement(int[] colormap, int startIndex,
			int index, int red, int green, int blue) {
		colormap[startIndex + index / 2] = Color.rgb(red, green, blue);
	}
}
